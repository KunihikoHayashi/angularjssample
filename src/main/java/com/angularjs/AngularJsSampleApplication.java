package com.angularjs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

//（サンプルプログラムを作成する上での参考サイト）
//http://masatoshitada.hatenadiary.jp/entry/2014/12/20/164258
//http://spring.io/guides/gs/consuming-rest-angularjs/
//http://m12i.hatenablog.com/entry/2015/03/08/114226
@SpringBootApplication
public class AngularJsSampleApplication {

    public static void main(String[] args) {
        SpringApplication.run(AngularJsSampleApplication.class, args);
    }
}
