package com.angularjs.api;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/")
public class RestApiController {

	//curl -X GET "http://localhost:8080/api/"
	@RequestMapping(method = RequestMethod.GET)
	ResponseEntity<?> getRestApi() {
      return new ResponseEntity<>(null,new HttpHeaders(),HttpStatus.FOUND);
	}

}
