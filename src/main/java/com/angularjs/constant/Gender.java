package com.angularjs.constant;

public enum Gender {
    MALE("男性"),
    FEMALE("女性");

    private final String value;

    private Gender(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
