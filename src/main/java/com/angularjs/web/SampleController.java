package com.angularjs.web;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/")
public class SampleController {

	//curl http://localhost:8080/ -v -X GET
	@RequestMapping(value="", method = RequestMethod.GET)
	String index(Model model) {
	    return "index";
	}

}
