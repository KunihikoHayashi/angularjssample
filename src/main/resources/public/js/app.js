angular.module("ToDo", ["Controllers", "Directives"], ["$routeProvider", function($routeProvider){
  $routeProvider.when("/", {
    controller : "MainCtrl",
    templateUrl : "/partials/main.html"
  });
}]);
