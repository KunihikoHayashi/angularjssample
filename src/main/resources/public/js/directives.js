angular.module("Directives", []).directive("addTodo", function(){
  return {
    restrict : "A", //はじめてのAngularJS P70 <div add-todo></div>にて使用可能
    replace : true, //はじめてのAngularJS P72
    scope : { todos : "=todos" }, //はじめてのAngularJS P73
    template : "<div><input ng-model='text'><button>追加</button></div>",
    link : function(scope, element) {
      var button = angular.element(element.children()[1]);
      button.bind("click", function(){
        scope.todos.push({text : scope.text, done : false});
        scope.text = "";
        scope.$apply();
      });
    }
  };
});
