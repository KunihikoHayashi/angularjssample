angular.module("Controllers", []).controller("MainCtrl", ["$scope", function($scope){
  $scope.title = "Todo";
  $scope.todos = [
    {text : "AngularJSを勉強する", done : true},
    {text : "AngularJSでアプリを作る", done : false}
  ];
  $scope.doneType = "line";
}]);
