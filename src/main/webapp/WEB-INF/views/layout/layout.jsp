<%-- JSPの文字化け対策--%>
<%-- http://www.atmarkit.co.jp/ait/articles/0412/25/news006.html--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>${param.title}</title>
    <link rel="stylesheet" href="/webjars/bootstrap/3.2.0/css/bootstrap.min.css" />
    <link rel="stylesheet" href="/webjars/bootstrap/3.2.0/css/bootstrap-theme.min.css" />
</head>
<body>
<%-- <jsp:include page="header.jsp"/> --%>
<h1>${param.h1}</h1>
<div id="main">
${param.content}
</div>
<%-- <jsp:include page="layout/footer.jsp"/>--%>
<script src="/webjars/jquery/1.11.1/jquery.min.js"></script>
<script src="/webjars/bootstrap/3.2.0/js/bootstrap.min.js"></script>
</body>
</html>