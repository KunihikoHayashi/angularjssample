<%@ page language="java" contentType="text/html; charset=UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="function" uri="http://java.sun.com/jsp/jstl/functions"%>
<%--@ taglib prefix="xml" uri="http://java.sun.com/jsp/jstl/xml"--%>
<%--@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"--%>
<%--(参考)http://struts.wasureppoi.com/jstl/01_use.html--%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<jsp:include page="../layout/layout.jsp">
    <jsp:param name="title" value="顧客一覧" /> 
    <jsp:param name="h1" value="顧客管理システム" /> 
    <jsp:param name="content">
    <jsp:attribute name="value">
    <div class="col-sm-12">
    <form:form modelAttribute="customerForm" action="/customers/create" class="form-horizontal">
        <fieldset>
            <legend>顧客情報作成</legend>
            <!-- Spring MVCでBootsrapを利用する場合の問題点 -->
            <!-- http://d.hatena.ne.jp/tatsu-no-toshigo/20140503/1399093728 -->
            <!-- 解決策 spring:bind タグの使用 -->
            <!-- http://www.jxpath.com/springWeb/step3/springTag.html -->
            <!-- http://hamasyou.com/blog/2004/11/06/spring-framework-mvc/ -->
            <!-- http://c9katayama.hatenablog.com/entry/20080805/1217897526 -->
            <!-- http://docs.spring.io/spring-framework/docs/2.5.x/api/org/springframework/web/servlet/support/BindStatus.html -->
            <spring:bind path="lastName">
            <div class="form-group ${status.isError() ? 'has-error has-feedback' : ''}">
                <form:label path="lastName" class="col-sm-2 control-label">姓</form:label>
                <div class="col-sm-10">
                    <form:input path="lastName" class="form-control" />
                    <form:errors path="lastName" cssStyle="color:red" element="span" class="help-block"/>
                </div>
            </div>
            </spring:bind>
            <spring:bind path="firstName">
            <div class="form-group ${status.isError() ? 'has-error has-feedback' : ''}">
                <form:label path="firstName" class="col-sm-2 control-label">名</form:label>
                <div class="col-sm-10">
                    <form:input path="firstName" class="form-control" />
                    <form:errors path="firstName" cssStyle="color:red" element="span" class="help-block"/>
                </div>
            </div>
            </spring:bind>
            <spring:bind path="gender">
            <div class="form-group ${status.isError() ? 'has-error has-feedback' : ''}">
                <form:label path="gender" class="col-sm-2 control-label">性別</form:label>
                <div class="col-sm-10">
                    <c:forEach var="item" items="${genderList}">
                    <label class="radio-inline"><form:radiobutton path="gender" class="radio-control" label="${item.value}" value="${item.name()}"/></label>
                    </c:forEach>
                    <form:errors path="gender" cssStyle="color:red" element="span" class="help-block"/>
                </div>
            </div>
            </spring:bind>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-primary">作成</button>
                </div>
            </div>
        </fieldset>
    </form:form>
    <hr/>
    <table class="table table-striped table-bordered table-condensed">
        <tr>
            <th>ID</th>
            <th>姓</th>
            <th>名</th>
            <th>性別</th>
            <th>編集</th>
            <th>削除</th>
        </tr>
        <c:forEach var="customer" items="${customers}">
        <tr>
            <td><c:out value="${customer.id}" /></td>
            <td><c:out value="${customer.lastName}" /></td>
            <td><c:out value="${customer.firstName}" /></td>
            <td><c:out value="${customer.gender.value}" /></td>
            <td>
                <form action="/customers/edit" method="get">
                    <input type="submit" name="form" value="編集" class="btn btn-default" />
                    <input type="hidden" name="id" value="${customer.id}"/>
                </form>
            </td>
            <td>
                <form action="/customers/delete" method="post">
                    <input type="submit" name="form" value="削除" class="btn btn-danger" />
                    <input type="hidden" name="id" value="${customer.id}" />
                </form>
            </td>
        </tr>
        </c:forEach>
    </table>
  </div>
  </jsp:attribute>
  </jsp:param>
</jsp:include>
