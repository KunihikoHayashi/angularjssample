package com.angularjs;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = AngularJsSampleApplication.class)
@WebAppConfiguration
public class AngularJsSampleApplicationTests {

	@Test
	public void contextLoads() {
	}

}
